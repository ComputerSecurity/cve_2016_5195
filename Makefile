all:

cve_2016_5195.tgz: Makefile cve_2016_5195.service cve_2016_5195.init
	tar -cvzf cve --transform 's,^,cve_2016_5195/,' $^ *.ko

cc7: 3.10.0-327.13.1.el7.x86_64_cve_2016_5195.ko 3.10.0-327.18.2.el7.x86_64_cve_2016_5195.ko 3.10.0-327.22.2.el7.x86_64_cve_2016_5195.ko 3.10.0-327.28.2.el7.x86_64_cve_2016_5195.ko 3.10.0-327.28.3.el7.x86_64_cve_2016_5195.ko 3.10.0-327.36.1.el7.x86_64_cve_2016_5195.ko 3.10.0-327.36.2.el7.x86_64_cve_2016_5195.ko

slc6: 2.6.32-642.6.1.el6.x86_64-cve_2016_5195.ko 2.6.32-642.4.2.el6.x86_64-cve_2016_5195.ko 2.6.32-642.3.1.el6.x86_64-cve_2016_5195.ko 2.6.32-642.1.1.el6.x86_64-cve_2016_5195.ko 2.6.32-573.8.1.el6.x86_64-cve_2016_5195.ko 2.6.32-573.7.1.el6.x86_64-cve_2016_5195.ko 2.6.32-573.26.1.el6.x86_64-cve_2016_5195.ko 2.6.32-573.22.1.el6.x86_64-cve_2016_5195.ko 2.6.32-573.18.1.el6.x86_64-cve_2016_5195.ko 2.6.32-573.12.1.el6.x86_64-cve_2016_5195.ko

slc5: 2.6.18-410.el5.x86_64-cve_2016_5195.ko 2.6.18-411.el5.x86_64-cve_2016_5195.ko 2.6.18-412.el5.x86_64-cve_2016_5195.ko

%-cve_2016_5195.ko: CVE-2016-5195.stp
	yum -y erase kernel-debuginfo kernel-debuginfo-common-x86_64
	yum -y --enablerepo 'centos*debug' --enablerepo 'slc6-*' --enablerepo 'slc5-*' install kernel-$* kernel-devel-$* kernel-debuginfo-$* systemtap systemtap-devel
	stap -p 4 -g -m cve_2016_5195 -r $* CVE-2016-5195.stp
	mv cve_2016_5195.ko  $$(echo $* | sed 's/[.-]/_/g')_cve_2016_5195.ko
	yum -y erase kernel-$* kernel-devel-$* kernel-debuginfo-$* kernel-debuginfo-common-x86_64
