Summary: CVE-2016-5195 temporary fix.
Name: cve_2016_5195
Version: 0.4
Vendor: CERN
Release: 1%{?dist}
License: GPL
Group: System
Source: %{name}.tgz
BuildRoot: /var/tmp/%{name}-build
Packager: Vincent Brillault <vincent.brillault@cern.ch>
Requires: systemtap-runtime
ExclusiveArch: x86_64

Requires: initscripts

%if 0%{?rhel} == 5
Requires: kernel >= 2.6.18-410.el5.x86_64
%endif

%if 0%{?rhel} == 6
Requires: kernel >= 2.6.32-573.7.1.el6.x86_64
%endif

%if 0%{?rhel} == 7
Requires: kernel >= 3.10.0-327.22.2.el7.x86_64
%endif

%description
SystemTap module fixing CVE-2016-5195


%if 0%{?rhel} == 7
%global supported_kernels 3.10.0-327.22.2.el7.x86_64 3.10.0-327.28.2.el7.x86_64 3.10.0-327.28.3.el7.x86_64 3.10.0-327.36.1.el7.x86_64 3.10.0-327.36.2.el7.x86_64
%endif

%if 0%{?rhel} == 6
%global supported_kernels 2.6.32-573.7.1.el6.x86_64 2.6.32-573.8.1.el6.x86_64 2.6.32-573.12.1.el6.x86_64 2.6.32-573.18.1.el6.x86_64 2.6.32-573.22.1.el6.x86_64 2.6.32-573.26.1.el6.x86_64 2.6.32-642.1.1.el6.x86_64 2.6.32-642.3.1.el6.x86_64 2.6.32-642.4.2.el6.x86_64 2.6.32-642.6.1.el6.x86_64
%endif

%if 0%{?rhel} == 5
%global supported_kernels 2.6.18-410.el5.x86_64 2.6.18-411.el5.x86_64 2.6.18-412.el5.x86_64
%endif

%prep
%setup -n %{name}

%build
# Due to restriction on the number of installed kernel-debuginfo, we can't built the modules here
# Please see the Makefile

# Don't strip my debuginfo!!
%define __strip /bin/true

%install
%if 0%{?rhel} == 7
mkdir -p  $RPM_BUILD_ROOT/usr/lib/systemd/system/
install -m0644 cve_2016_5195.service $RPM_BUILD_ROOT/usr/lib/systemd/system/
%endif

%if 0%{?rhel} <= 6
mkdir -p $RPM_BUILD_ROOT/etc/init.d/
install -m0755 cve_2016_5195.init $RPM_BUILD_ROOT/etc/init.d/cve_2016_5195
%endif

for kernel in %supported_kernels; do
	mkdir -p $RPM_BUILD_ROOT/lib/modules/$kernel/extra/
	install -m0644  `echo $kernel | sed 's/[.-]/_/g'`_cve_2016_5195.ko $RPM_BUILD_ROOT/lib/modules/$kernel/extra/cve_2016_5195.ko
done

%files
%defattr(-,root,root)
%if 0%{?rhel} == 7
/usr/lib/systemd/system/cve_2016_5195.service
/lib/modules/
%endif

%if 0%{?rhel} <= 6
/etc/init.d/cve_2016_5195
/lib/modules/
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
echo "loading SystemTap cve_2016_5195 module"

%if 0%{?rhel} == 7
/usr/bin/systemctl daemon-reload 
/usr/bin/systemctl enable cve_2016_5195
/usr/bin/systemctl start cve_2016_5195
%endif

%if 0%{?rhel} <= 6
/sbin/chkconfig --add cve_2016_5195 || :
/sbin/service cve_2016_5195 start || :
%endif

%preun
%if 0%{?rhel} == 7
/usr/bin/systemctl stop cve_2016_5195
/usr/bin/systemctl disable cve_2016_5195
/usr/bin/systemctl daemon-reload
%endif

%if 0%{?rhel} <= 6
/sbin/service cve_2016_5195 stop || :
/sbin/chkconfig --del cve_2016_5195 || :
%endif
rpm -ql cve_2016_5195-%{version}-%{release} | grep '\.ko$' > /var/run/rpm-kmod-cve_2016_5195-%{version}-%{release}-modules

%postun
modules=( $(cat /var/run/rpm-kmod-cve_2016_5195-%{version}-%{release}-modules) )
rm /var/run/rpm-kmod-cve_2016_5195-%{version}-%{release}-modules
if [ -x "/sbin/weak-modules" ]; then
    printf '%s\n' "${modules[@]}"     | /sbin/weak-modules --remove-modules
fi

%changelog
* Thu Oct 27 2016 Vincent Brillault <vincent.brillault@cern.ch> 0.4
- Ensure that weak-update links are removed

* Sun Oct 23 2016 Thomas Oulevey <thomas.oulevey@cern.ch> 0.3
- Support el5 x86_64

* Fri Oct 21 2016 Thomas Oulevey <thomas.oulevey@cern.ch> 0.3
- Support el6 x86_64
- Fix systemd service

* Thu Oct 20 2016 Vincent Brillault <vincent.brillault@cern.ch> 0.2
- Include service & post/preun

* Thu Oct 20 2016 Vincent Brillault <vincent.brillault@cern.ch> 0.1
- initial release
